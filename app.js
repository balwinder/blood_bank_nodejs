const  express = require ('express');
const app = express();
const morgan = require('morgan');
const bodyparser = require('body-parser');
const mongoose = require('mongoose');
const productRouts = require('./api/routs/production');
const orderrouts = require('./api/routs/orders');
const userRouts = require('./api/routs/user');
const chatRouts = require('./api/routs/chat');
const cors = require('cors');
 //app.use(cors());
 //app.options('*', cors());
 mongoose.connect('mongodb://localhost/test',function(err){
     if(err){
         console.log(err);
     }else{
         console.log('Connected to mongodb');
     }
 })
// mongoose.connect('mongodb+srv://balwinder:'+ process.env.MONGO_ATLAS_PW + '@node-rest-stop-1yg8f.mongodb.net/test?retryWrites=false'
// );
app.use(morgan('dev'));
app.use(bodyparser.urlencoded({extended:false}));
app.use(bodyparser.json());
app.use('/production',productRouts);
app.use('/orders',orderrouts);
app.use('/user',userRouts);
app.use('/chat',chatRouts);

//comented iteem
app.use((req,res,next) =>{
    
   res.header('Access-Control-Allow-Origin','*');
   res.header('Access-Control-Allow-Header','Origin,X-Requested-with,Content-Type,Authorization');
   if(re.method =='OPTION'){
       res.header('Access-Control-Allow-Methods','PUT,POST,PATCH,DELETE ,GET');
       return res.status(200),json({});
   }
   next();
});

app.use((req,res,next)=>{
   
    const error = new Error('not found');
    error.status = 404;
    next(error);
})
app.use((error,req,res,next) => {


     res.status(error.status || 500);
     
     
     res.json({
         error :{
            
             message :error.message
         }
     })
});
//socket


module.exports = app;