const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Order = require('../models/order');
const Product = require('../models/product');
const checKAuth = require('../middleware/check-auth');
const ordercontroller =require('../controllers/order');
//get orders
router.get('/',checKAuth,ordercontroller.orders_get_all);
     
router.post('/',checKAuth,(req,res,next) => {
   Product.findById(req.body.product).then(product =>{
    if(!product){
        return res.status(404).json({
            message:"Product not found"
        });
    }
    const order = new Order({
        _id :mongoose.Types.ObjectId(),
        quantity:req.body.quantity,
        product:req.body.product
    });
    return order.save()

   }).then(result => {
       
    console.log(result);
    res.status(201).json({
        message :"order stored",
        createdOrder:{
    _id:result.id,
    product:result.product,
    quantity:result.quantity
        },
        type:{
          type:'GET',
          url:'http://localhost:3000/orders/'+result._id

        }
    });
}).catch(err => {
    console.log(err);
    res.status(500).json({
        error:err
        
        
    });


    
});
    
  //  {
    //     productid : req.body.productid,
    //     quantity :req.body.quantity
    // }

    
});

router.get('/:orderid',checKAuth,(req,res,next) => {


   Order.findById(req.params.orderid).populate('product').exec().then(order =>{
       if(!order){
           return res.status(404).json({
               message:"order not found"
           })
       }
       res.status(200).json({
           order:order,
           request:{
               type:'GET',
               url:"http://localhost:3000/orders"
           }
       });
      
   }).catch(err =>{
       res.status(500).json({
           error:err
       });
   });

});
router.delete('/:orderid',checKAuth,(req,res,next) => {
Order.remove({_id:req.params.orderid}).exec().then(result =>{
    res.status(200).json({
     message:'order deleted',
     request:{
         type:"GET",
         http:'localhost:3000/orders',
         body:{
             productid:'ID',quantity:'Number'
         }
     }
    });
}).catch(err =>{
    res.status(500).json({
        error:err
    });
});
});

module.exports = router;