const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const jwt = require("jsonwebtoken");
const bcrypt = require('bcrypt');
const User2 = require('../models/user');
const checKAuth = require('../middleware/check-auth');

//socket
router.post('/signup',(req,res,next)=>{

       User2.find({email:req.body.email}).exec().then(user =>{
           if(user.length>=1){
            console.log("mail exists");
               return res.status(201).json({
                   message:"Mailexists"
                 
               })
           }else{
            bcrypt.hash(req.body.password,10,(err,hash)=>{
                if(err){
                    return res.status(500).json({
                        error:err
                        
                    });
                }else{
                    const user = new User2({
                        _id:new mongoose.Types.ObjectId(),
                        email:req.body.email,
                        password:hash,
                        bloodgroup:req.body.bloodgroup,
                        gender:req.body.gender,
                        name:req.body.name,
                        phonenumber:req.body.phonenumber,
                        Coordinates:req.body.Coordinates  
                      });
                    user.save().then(result=>{
               console.log(result);
                      res.status(201).json({
                          message:'User created'
                      });
                    }).catch(err =>{
                        console.log(err);
                        res.status(500).json({
                            error: err
                          })
        
                    });
                }
            });
           }
       })
        
});

   router.post('/login',(req,res,next)=>{
       User2.find({email:req.body.email}).exec().then(user=>{
           if(user<1){
               return res.status(401).json({
                   message:"Auth failed"
               });
           }
           bcrypt.compare(req.body.password,user[0].password,(err,result) =>{
             if(err){
                 return res.status(401).json({
                     message:'Auth failed'

                 });
             }
             if(result){
                const token = jwt.sign({
                     email:user[0].email,
                     userId:user[0]._id
                 },
                 process.env.JWT_KEY,
                {
                 expiresIn:"1hr"
                },

            );
                   return res.status(200).json({
                       message:'Auth Succesfull',
                       token :token
                   });
             }
             res.status(401).json({
                message:'Auth failed'

            });
           });

       }).catch(err=>{
        console.log(err);
        res.status(500).json({
            error:err
        });
    });
   });

router.delete('/',(req,res,next)=>{
    User.remove().exec().then(result =>{
        res.status(200).json({
            message:'user deleted'
        });
    }).catch(err=>{
        console.log(err);
        res.status(500).json({
            error:err
        });
    });
});



router.get('/',(req,res,next) => {
    console.log("heloo");
    const bloodgroup = req.params.bloodgroup;
     User2.find().select('bloodgroup Coordinates')
    
            .exec().then(
        docs =>{
            const response = {
                count:docs.length,
                User2:docs.map(doc =>{
                    return{
                        email :doc.email,
                        password :doc.password,
                        bloodgroup:doc.bloodgroup,
                        gender:doc.gender,
                        phonenumber:doc.phonenumber,
                        name:doc.name,
                       Coordinates:doc.Coordinates,
                    _id: doc._id,
                    request:{
                        type:'GET',
                        url:'http://localhost:3000/user/login'+doc._id
                    }

                    }
                })
            };
         
                res.status(200).json(response)
           
           
        }
    ).catch(err=>{
        console.log(err);
        res.status(500).json({
            error:err
        });
    });
    
});


router.get('/:bloodgroup',checKAuth,(req,res,next)  => {
    const bloodgroup = req.params.bloodgroup;
    User2.aggregate([
        {   
            $geoNear:{
                            near: { type: "Point", coordinates: [ parseFloat(req.query.lng),parseFloat(req.query.lat)]},
                            distanceField: "dist.calculated",
                            spherical: true,
                            maxDistance:1000000000000
                            //minDistance: 0, maxDistance: maxDistance,
            }
           
        },
        {$match:{bloodgroup:{
             $regex:new RegExp(bloodgroup)
        }}}
    ] 
)
    .exec().
    then(
        docs =>{
            const response = {
                count:docs.length,
                User2:docs.map(doc =>{
                    return{
                        email :doc.email,
                        password :doc.password,
                        bloodgroup:doc.bloodgroup,
                        gender:doc.gender,
                        phonenumber:doc.phonenumber,
                        name:doc.name,
                        Coordinates:doc.Coordinates,
                    _id: doc._id,
                    request:{
                        type:'GET',
                        url:'http://localhost:3000/user/login'+doc._id
                    }

                    }
                })
            };
                res.status(200).json(response)
     
        }
    ).catch(err=>{
        console.log(err);
        res.status(500).json({
            error:err
        });
    });
    
});
//socket

router.post('/name',(req,res,next)  => {
    const name = req.params.name;
    User2.find({name:req.body.name}).
    exec().
    then(
        docs =>{
            const response = {
                count:docs.length,
                User2:docs.map(doc =>{
                    return{
                        email :doc.email,
                        password :doc.password,
                        bloodgroup:doc.bloodgroup,
                        gender:doc.gender,
                        phonenumber:doc.phonenumber,
                        name:doc.name,
                       Coordinates:doc.Coordinates,
                    _id: doc._id,
                    request:{
                        type:'GET',
                        url:'http://localhost:3000/user/login'+doc._id
                    }
   
                    }
                })
            };
           
                res.status(200).json(response) 
        }
    ).
    catch(err =>{
        console.log(err);
      res.status(500).json({error:err});
    });
 
    
    });


//socket



module.exports = router;