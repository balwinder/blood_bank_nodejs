const mongoose  = require('mongoose');
const Geoschema = new mongoose.Schema ({
    type:{
        type:String,
        default:'Point'
    },
    Coordinates:{
        type:[Number],
        index:"2dsphere"
    }
});

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    
   email:{type:String,required:true,unique:true,
    match:/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/},
   password:{type:String,required:true},
   bloodgroup:{type:String,required:true},
   gender:{type:String},
   name:{type:String,required:true},
   phonenumber:{type:String,required:true},
  // location: { type: "Point", coordinates: [String,String ] }
   //geometry:Geoschema
   Coordinates:{
    type:[Number],
    index:"2dsphere"
}
    
});


//create geolocation schema




module.exports = mongoose.model('User2',userSchema);